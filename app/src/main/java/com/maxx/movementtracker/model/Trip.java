package com.maxx.movementtracker.model;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mayank Gautam
 *         Created: 01/12/17
 */

public class Trip extends Model implements Parcelable {

    public static final Creator<Trip> CREATOR = new Creator<Trip>() {
        @Override
        public Trip createFromParcel(Parcel in) {
            return new Trip(in);
        }

        @Override
        public Trip[] newArray(int size) {
            return new Trip[size];
        }
    };
    public List<LatLngTime> locations;
    public double distance = 0;
    public boolean tripCompleted = false;

    public Trip(){
        locations = new ArrayList<>();
    }

    protected Trip(Parcel in) {
        super(in);
        locations = in.createTypedArrayList(LatLngTime.CREATOR);
        tripCompleted = in.readByte() != 0;
    }

    public static float getDistanceBetween(LatLngTime location1, LatLngTime location2) {
        float[] results = new float[1];
        Location.distanceBetween(location1.getLatitude(),
                location1.getLongitude(),
                location2.getLatitude(),
                location2.getLongitude(),
                results);
        return results[0];
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(locations);
        dest.writeByte((byte) (tripCompleted ? 1 : 0));
    }

    public void addLocation(LatLngTime location){
        if (locations.size() > 0)
            distance += getDistanceBetween(getTripEnd(), location);
        locations.add(location);
    }

    public void addLocation(int index, LatLngTime location) {
        if (locations.size() > 0)
            distance += getDistanceBetween(locations.get(index - 1), location);
        locations.add(index, location);
    }

    @JsonIgnore
    public LatLngTime getTripStart(){
        return locations.get(0);
    }

    @JsonIgnore
    public LatLngTime getTripEnd(){
        return locations.get(locations.size()-1);
    }
}
