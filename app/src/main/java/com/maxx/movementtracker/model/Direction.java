package com.maxx.movementtracker.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * A wrapper for parsing the Google direction's api object.
 * @author Mayank Gautam
 *         Created: 03/12/17
 */

public class Direction extends Model implements Parcelable {
    public static final Creator<Direction> CREATOR = new Creator<Direction>() {
        @Override
        public Direction createFromParcel(Parcel in) {
            return new Direction(in);
        }

        @Override
        public Direction[] newArray(int size) {
            return new Direction[size];
        }
    };

    public List<Route> routes;

    public Direction() {

    }

    protected Direction(Parcel in) {
        super(in);
        routes = in.createTypedArrayList(Route.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(routes);
    }
}
