package com.maxx.movementtracker.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @author Mayank Gautam
 *         Created: 02/12/17
 */
@JsonPropertyOrder({
        "error",
        "error_description"
})
public class NetworkError extends Model implements Parcelable {
    public static final String SOCKET_TIMEOUT = "socket_timeout";
    public static final Creator<NetworkError> CREATOR = new Creator<NetworkError>() {
        @Override
        public NetworkError createFromParcel(Parcel in) {
            return new NetworkError(in);
        }

        @Override
        public NetworkError[] newArray(int size) {
            return new NetworkError[size];
        }
    };
    @JsonProperty("error")
    private String name;
    @JsonProperty("error_description")
    private String descr;

    public NetworkError() {
    }

    protected NetworkError(Parcel in) {
        name = in.readString();
        descr = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(descr);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @JsonProperty("error")
    public String getName() {
        return name;
    }

    @JsonProperty("error")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("error_description")
    public String getDescr() {
        return descr;
    }

    @JsonProperty("error_description")
    public void setDescr(String code) {
        this.descr = code;
    }
}