package com.maxx.movementtracker.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.android.gms.maps.model.LatLng;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 *
 * This model encapsulated Google api's {@link LatLng} to add additional timestamp component.
 * @author Mayank Gautam
 *         Created: 01/12/17
 */

public class LatLngTime extends Model implements Parcelable{

    public static final Creator<LatLngTime> CREATOR = new Creator<LatLngTime>() {
        @Override
        public LatLngTime createFromParcel(Parcel in) {
            return new LatLngTime(in);
        }

        @Override
        public LatLngTime[] newArray(int size) {
            return new LatLngTime[size];
        }
    };
    @JsonIgnore
    private LatLng latLng;
    @JsonIgnore
    private long timestamp;

    @JsonCreator
    public LatLngTime(@JsonProperty("latitude") double latitude,
                      @JsonProperty("longitude") double longitude,
                      @JsonProperty("timestamp") long timestamp) {
        latLng = new LatLng(latitude,longitude);
        this.timestamp = timestamp;
    }

    protected LatLngTime(Parcel in) {
        super(in);
        latLng = in.readParcelable(LatLng.class.getClassLoader());
        timestamp = in.readLong();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(latLng, flags);
        dest.writeLong(timestamp);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    //These functions are required to convert the object to Json Object.
    @JsonProperty("latitude")
    public double getLatitude(){
        return latLng.latitude;
    }

    @JsonProperty("longitude")
    public double getLongitude(){
        return latLng.longitude;
    }

    @JsonProperty("timestamp")
    public long getTimestamp(){
        return timestamp;
    }


    /**
     * This functions converts the timestamp to the date format to be shown in {@link com.maxx.movementtracker.controller.TripAdapter.ViewHolder}
     *
     * @return The date in specified format
     */
    @JsonIgnore
    public String getDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM dd, KK:mm aa", Locale.getDefault());
        return sdf.format(new Date(timestamp));
    }


    @JsonIgnore
    public LatLng getLatLng(){
        return latLng;
    }
}
