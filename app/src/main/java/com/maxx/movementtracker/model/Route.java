package com.maxx.movementtracker.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;

import java.util.List;

/**
 * @author Mayank Gautam
 *         Created: 03/12/17
 */

public class Route extends Model implements Parcelable {
    public static final Creator<Route> CREATOR = new Creator<Route>() {
        @Override
        public Route createFromParcel(Parcel in) {
            return new Route(in);
        }

        @Override
        public Route[] newArray(int size) {
            return new Route[size];
        }
    };
    @JsonIgnore
    public List<LatLng> latLngs;

    public Route() {

    }

    protected Route(Parcel in) {
        super(in);
        latLngs = in.createTypedArrayList(LatLng.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(latLngs);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @JsonProperty("overview_polyline")
    public void setOverviewPolyline(Polyline polyline) {
        latLngs = PolyUtil.decode(polyline.points);
    }

    //Class only to be used to read polyline from directions API
    class Polyline {

        @JsonProperty("points")
        public String points;

        public Polyline() {

        }
    }
}
