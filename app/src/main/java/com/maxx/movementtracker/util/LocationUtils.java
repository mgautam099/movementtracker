package com.maxx.movementtracker.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.Task;
import com.maxx.movementtracker.Network.NetworkUtils;
import com.maxx.movementtracker.Network.OnLoadFinishListener;
import com.maxx.movementtracker.model.LatLngTime;
import com.maxx.movementtracker.model.Route;
import com.maxx.movementtracker.model.Trip;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;

/**
 * This class encapsulates the utility methods used by the Application.
 *
 * @author Mayank Gautam
 *         Created: 01/12/17
 */

public class LocationUtils {

    private static final int REQUEST_CHECK_SETTINGS = 1;

    //Location interval in milliseconds
    private static final long LOCATION_INTERVAL = 2000; //in milli
    private static final long FASTEST_LOCATION_INTERVAL = 500; //in milli


    // Increasing this value will make the app track more of user's movements, but will make
    // app susceptible to erroneous locations and/or Outliers.
    private static final float HORIZONTAL_ACCURACY = 25f; // in metres

    //This variable maintains the save rate for the trip, for example: if Save Factor is 10,
    //the trip will be saved to file every 10 location callbacks or 10 * Location interval milliseconds
    private static final int SAVE_FACTOR = 10;

    // This difference (Milliseconds) in locations will trigger action to get directions from google api
    // to map the lost route in case path is lost for some reason
    private static final long INTERVAL_BETWEEN_REQUEST = 40000; //in mili
    private static final float DISTANCE_BETWEEN_REQUEST = 100f; // in metres

    // Maximum failed attempt counts, after every interval of this number a Toast is
    // shown for checking GPS
    private static final int FAILED_ATTEMPT_COUNT = 20;

    private LocationRequest mLocationRequest;
    private MapUtil mMapUtil;

    //Context of the current Activity.
    private Context mContext;

    private Trip currentTrip;

    // Variable to maintain in case GPS location cannot be locked.
    private int consecutiveFailedAttempts = 1;

    // This map will be used in case points are being resumed in a trip and directions are requested
    // from google directions API, once the directions are retrieved all the missing latitudes in
    // between will be added and this will be cleared
    @SuppressLint("UseSparseArrays")
    private Map<Long, Integer> indexMap = new HashMap<>();

    private ArrayList<Trip> previousTrips = new ArrayList<>();
    public LocationCallback mLocationCallBack = new LocationCallback(){
        @Override
        public void onLocationResult(LocationResult locationResult) {
            captureLocations(locationResult);
        }
    };

    public LocationUtils(Context context) {
        mContext = context;
    }

    public LocationRequest getLocationRequest(){
        if(mLocationRequest == null){
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(LOCATION_INTERVAL);
            mLocationRequest.setFastestInterval(FASTEST_LOCATION_INTERVAL);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        }
        return mLocationRequest;
    }

    public void setMap(MapUtil mapUtil){
        mMapUtil = mapUtil;
    }

    public void checkLocationSettings(Activity activity){
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(getLocationRequest());
        SettingsClient client = LocationServices.getSettingsClient(activity);
        Task<LocationSettingsResponse> settingsTask = client.checkLocationSettings(builder.build());
        settingsTask.addOnSuccessListener(activity,locationSettingsResponse -> {

        });
        settingsTask.addOnFailureListener(activity,e -> {
           int statusCode = ((ApiException)e).getStatusCode();
           switch (statusCode){
               case CommonStatusCodes.RESOLUTION_REQUIRED:
                   // Settings changed are required, hence need to ask user to change them for us by
                   // showing a dialog
                   try{
                       ResolvableApiException apiException = (ResolvableApiException) e;
                       apiException.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS);
                   } catch (IntentSender.SendIntentException ignored){
                       //ignore the exception
                   }
                   break;

               case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                   //No way to change the location hence show the user the dialog to terminate the app
                   // or that the app cannot work for him
                   break;
           }
        });
    }

    /**
     * Method called by Callback when location is available for the app.
     *
     * @param locationResult result containing the list of locations since last callback.
     */
    public void captureLocations(LocationResult locationResult){
        if(currentTrip==null)
            return;
        LatLngTime origin = null;
        if (currentTrip.locations.size() > 0) {
            origin = currentTrip.getTripEnd();
        }
        for (int i = 0; i < locationResult.getLocations().size(); i++) {
            Location location = locationResult.getLocations().get(i);

            //Locations with low accuracy will be ignored.
            if(location.getAccuracy()<HORIZONTAL_ACCURACY) {
                LatLngTime latLngTime = new LatLngTime(location.getLatitude(), location.getLongitude(), location.getTime());
                currentTrip.addLocation(latLngTime);
                if (origin != null && i == 0) {
                    getDirectionsBetween(origin, latLngTime);
                }
                consecutiveFailedAttempts = 1;
            } else {
                consecutiveFailedAttempts++;
            }
        }
        if (consecutiveFailedAttempts % FAILED_ATTEMPT_COUNT == 0) {
            Toast.makeText(mContext, "Location accuracy too low, please check your GPS or continue in an open area.", Toast.LENGTH_LONG)
                    .show();
        }
        // Saving trip at fixed intervals lets us resume the trip
        // from last saved point preventing us from at least losing the whole trip,
        // in case app is force closed.
        if (currentTrip.locations.size() == 1 || currentTrip.locations.size() % SAVE_FACTOR == 0) {
            saveTrip(false);
        }
        if (currentTrip.locations.size() > 0) {
            if (mMapUtil != null) {
                mMapUtil.drawTrip(currentTrip, false);
            }
        }
    }

    /**
     * Clear the map and start a new trip. The trip will start tracking after the horizontal accuracy
     * of {@link #HORIZONTAL_ACCURACY} is reached.
     */
    public void startTrip() {
        Toast.makeText(mContext, "Trip will start after getting lock on your position", Toast.LENGTH_SHORT).show();
        mMapUtil.clear();
        currentTrip = new Trip();
    }

    /**
     * Stop the drip and draw the last trip completely
     */
    public void stopTrip() {
        if(currentTrip==null)
            return;
        currentTrip.tripCompleted = true;
        if(mMapUtil!=null)
            mMapUtil.drawTrip(currentTrip, false);
        saveTrip(true);
        currentTrip = null;
    }

    /**
     * Continue last trip if the app was force closed
     *
     * @param currentLocation
     */
    public void continueTrip(Location currentLocation) {
        if (currentTrip == null)
            return;
//        if (currentLocation != null) {
//            LatLngTime dest = new LatLngTime(currentLocation.getLatitude(), currentLocation.getLongitude(), currentLocation.getTime());
//            LatLngTime origin = currentTrip.getTripEnd();
//            getDirectionsBetween(origin,dest);
//        }
        if (mMapUtil != null)
            mMapUtil.drawTrip(currentTrip, false);
    }

    /**
     * Calls the Directions API if the is a large gap between two locations and their timestamps
     * to determine the shortest route. This is mostly used in case when the trip is lost due to
     * force kill by system/user.
     *
     * @param origin
     * @param dest   the latest location which is received after update. Should have already
     *               been added to the trip
     */
    private void getDirectionsBetween(LatLngTime origin, LatLngTime dest) {
        if (dest.getTimestamp() - origin.getTimestamp() > INTERVAL_BETWEEN_REQUEST
                && Trip.getDistanceBetween(origin, dest) > DISTANCE_BETWEEN_REQUEST) {
            NetworkUtils.getDirections(origin, dest, (OnLoadFinishListener) mContext);

            //Will give wrong result if called before adding destination to the list.
            indexMap.put(origin.getTimestamp(), currentTrip.locations.size() - 2);
            indexMap.put(dest.getTimestamp(), currentTrip.locations.size() - 1);
        }
    }


    public Trip getCurrentTrip() {
        return currentTrip;
    }

    public void saveTrip(boolean postData) {
        if(currentTrip.locations.size()<1)
            return;
        if (currentTrip.tripCompleted)
            previousTrips.add(0, currentTrip);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.writeValue(new File(mContext.getFilesDir(), "Trip_" + currentTrip.getTripStart().getTimestamp()), currentTrip);
        } catch (Exception e) {
            Log.e("LocationUtils","Saving Trip Failed: %s",e);
        }
        if (postData)
            NetworkUtils.sendTrip(currentTrip, (OnLoadFinishListener) mContext);
    }


    public void readAllTrips(Context context){
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            for(File file: context.getFilesDir().listFiles((dir, name) ->
                    name.startsWith("Trip"))){
                    Trip trip = objectMapper.readValue(file,Trip.class);
                if (trip.tripCompleted)
                    previousTrips.add(0, trip);
                else
                    currentTrip = trip;
            }
        } catch (IOException e) {
            Log.e("LocationUtils","Reading Trips Failed: %s",e);
        }
    }

    public ArrayList<Trip> getAllTrips() {
        return previousTrips;
    }

    public boolean isTripOnGoing() {
        return !(currentTrip == null || currentTrip.tripCompleted);
    }

    public void updatePaths(LatLngTime origin, LatLngTime dest, Route route) {
        int index = indexMap.get(dest.getTimestamp());
        if (currentTrip.locations.get(index).getTimestamp() != dest.getTimestamp()) {
            Log.d("LocationUtils:", "No Longer on same trip");
            //Todo find and update the trip missing locations from history.
            return;
        }
        ListIterator<LatLng> li = route.latLngs.listIterator(route.latLngs.size());
        while (li.hasPrevious()) {
            LatLng loc = li.previous();
            LatLngTime latLngTime = new LatLngTime(loc.latitude, loc.longitude, dest.getTimestamp());
            currentTrip.addLocation(index, latLngTime);
        }
        if (route.latLngs.size() > 0) {
            if (mMapUtil != null) {
                mMapUtil.clear();
                mMapUtil.drawTrip(currentTrip, false);
            }

        }
    }
}
