package com.maxx.movementtracker.util;

import android.graphics.Color;
import android.support.annotation.NonNull;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.maxx.movementtracker.R;
import com.maxx.movementtracker.model.LatLngTime;
import com.maxx.movementtracker.model.Trip;

/**
 * This Utility class encapsulates all the functions required to interact with {@link GoogleMap}
 * instance. Will generally be instantiated after the callback
 * {@link com.google.android.gms.maps.OnMapReadyCallback#onMapReady}
 *
 * @author Mayank Gautam
 *         Created: 01/12/17
 */

public class MapUtil {

    // Initial zoom for CameraOptions. Greater the value, higher the zoom.
    public static final float DEFAULT_ZOOM = 18;

    // Options to draw user movement on map.
    private PolylineOptions mPolylineOptions;

    // Options to configure for Start and End Marker
    private MarkerOptions startMarker;
    private MarkerOptions endMarker;


    private GoogleMap mMap;

    //To prevent redrawing of same location repeatedly.
    private int lastDrawnIndex = 0;

    /**
     * Requires Current {@link GoogleMap} instance.
     *
     * @param map The maps instance attached to the calling Fragment/Activity. Cannot be null.
     */
    public MapUtil(@NonNull GoogleMap map) {
        mMap = map;
        initOptions();
    }

    /**
     * Initialises the line and marker options, and sets the styling parameters.
     */
    private void initOptions(){
        mPolylineOptions = new PolylineOptions();
        mPolylineOptions.color(Color.parseColor("#40C4FF"));
        startMarker = new MarkerOptions();
        endMarker = new MarkerOptions();
        startMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker_green));
        endMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker_red));
    }

    /**
     * Incase you just want to add a marker to the Map.
     * @param latLngTime {@link LatLngTime} reference to add the Marker.
     * @param markerLabel {@link String} label to be shown on the marker.
     */
    public void addMarkerAndMove(LatLngTime latLngTime, String markerLabel){
        mMap.addMarker(new MarkerOptions().position(latLngTime.getLatLng()).title(markerLabel));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngTime.getLatLng(),DEFAULT_ZOOM));
    }

    public void drawLocation(LatLngTime latLngTime){
        mPolylineOptions.add(latLngTime.getLatLng());
        mMap.addPolyline(mPolylineOptions);
    }

    /**
     * Method to be called in case drawing on the Map.
     *
     * @param trip       {@link Trip} to be drawn on Map
     * @param zoomToTrip boolean specifying if the camera should be moved/zoomed to cover
     *                   whole trip or just the latest location.
     */
    public void drawTrip(@NonNull Trip trip, boolean zoomToTrip) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        //Unusual condition in case trip has ended but map has not cleared.
        if(trip.locations.size()==0)
        {
            clear();
            return;
        }
        mMap.addMarker(startMarker.position(trip.getTripStart().getLatLng()).title("Start Point"));
        if(trip.tripCompleted)
            mMap.addMarker(endMarker.position(trip.getTripEnd().getLatLng()).title("End Point"));
        for(int i = lastDrawnIndex; i<trip.locations.size(); i++){
            mPolylineOptions.add(trip.locations.get(i).getLatLng());
            builder.include(trip.locations.get(i).getLatLng());
        }
        mMap.addPolyline(mPolylineOptions);

        if (zoomToTrip) {
            LatLngBounds bounds = builder.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 150);
            mMap.moveCamera(cu);
        } else {
            if (lastDrawnIndex != trip.locations.size())
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(trip.getTripEnd().getLatLng(), DEFAULT_ZOOM));
        }
        lastDrawnIndex = trip.locations.size();
    }

    public GoogleMap getMap() {
        return mMap;
    }

    /**
     * Clear the map and reset drawn index. Also reset the lines and markers and get them ready for
     * new trip
     */
    public void clear() {
        mMap.clear();
        lastDrawnIndex = 0;
        initOptions();
    }
}
