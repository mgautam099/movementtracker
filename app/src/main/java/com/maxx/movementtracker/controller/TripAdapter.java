package com.maxx.movementtracker.controller;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.maxx.movementtracker.R;
import com.maxx.movementtracker.fragment.TripHistoryFragment;
import com.maxx.movementtracker.model.Trip;

import java.text.DecimalFormat;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Trip} and makes a call to the
 * specified {@link TripHistoryFragment.TripInteractionListener}.
 */
public class TripAdapter extends RecyclerView.Adapter<TripAdapter.ViewHolder> {

    private final List<Trip> mTripList;
    private final TripHistoryFragment.TripInteractionListener mListener;

    public TripAdapter(List<Trip> items, TripHistoryFragment.TripInteractionListener listener) {
        mTripList = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_trip, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.setItem(mTripList.get(position));
    }

    @Override
    public int getItemCount() {
        return mTripList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        final View mView;
        final TextView mStartTripView;
        final TextView mEndTripView;
        final TextView mDistanceView;
        Trip mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            mStartTripView = view.findViewById(R.id.tv_start_time);
            mEndTripView = view.findViewById(R.id.tv_end_time);
            mDistanceView = view.findViewById(R.id.tv_distance_travelled);
            mView.setOnClickListener(this);
        }

        void setItem(Trip item) {
            mItem = item;
            mStartTripView.setText(item.getTripStart().getDate());
            mEndTripView.setText(item.getTripEnd().getDate());
            String distance;

            //If the distance is greater than 1000 meters it is displayed in Kilometers
            if (item.distance < 1000)
                distance = new DecimalFormat("#.#").format(item.distance) + " mtrs";
            else
                distance = new DecimalFormat("#.###").format((item.distance / 1000)) + " kms";
            mDistanceView.setText(distance);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mEndTripView.getText() + "'";
        }

        @Override
        public void onClick(View v) {
            if (mListener != null)
                mListener.onTripClicked(mItem);
        }
    }
}
