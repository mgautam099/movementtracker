package com.maxx.movementtracker.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.maxx.movementtracker.R;
import com.maxx.movementtracker.model.Trip;
import com.maxx.movementtracker.util.MapUtil;


/**
 * A Fragment used to show the selected past trip to the user.
 * Use the {@link TripFragment#newInstance} to create a newInstance of this Fragment
 */
public class TripFragment extends Fragment implements OnMapReadyCallback {
    private static final String ARG_TRIP = "trip";

    private Trip mTrip;
    private MapUtil mMapUtil;


    public TripFragment() {
        // Required empty public constructor
    }

    public static TripFragment newInstance(Trip trip) {
        TripFragment fragment = new TripFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_TRIP, trip);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState!=null){
            mTrip = savedInstanceState.getParcelable(ARG_TRIP);
        }
        else if (getArguments() != null) {
            mTrip = getArguments().getParcelable(ARG_TRIP);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_trip, container, false);
        ((TextView) view.findViewById(R.id.tv_trip_time)).setText("Trip on " + mTrip.getTripStart().getDate());
        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(ARG_TRIP,mTrip);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMapUtil = new MapUtil(googleMap);
        mMapUtil.drawTrip(mTrip, true);
    }
}
