package com.maxx.movementtracker.fragment;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.Task;
import com.maxx.movementtracker.R;
import com.maxx.movementtracker.TrackActivity;
import com.maxx.movementtracker.model.LatLngTime;
import com.maxx.movementtracker.model.Route;
import com.maxx.movementtracker.model.Trip;
import com.maxx.movementtracker.util.LocationUtils;
import com.maxx.movementtracker.util.MapUtil;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnTrackFragmentInteraction} interface
 * to handle interaction events.
 */
public class TrackFragment extends Fragment implements OnMapReadyCallback {


    private static final String TAG = "TrackFragment";
    private MapUtil mMapUtil;
    private LocationUtils mLocationUtils;
    private FusedLocationProviderClient mLocationClient;
    private OnTrackFragmentInteraction mListener;
    private Location mLastKnownLocation;

    public TrackFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mLocationUtils = new LocationUtils(getActivity());
        mLocationUtils.checkLocationSettings(getActivity());
        mLocationUtils.readAllTrips(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        Button buttonTrip = view.findViewById(R.id.button_trip);
        buttonTrip.setText(mLocationUtils.isTripOnGoing() ? R.string.stop_trip : R.string.start_trip);
        buttonTrip.setOnClickListener(v -> startStopTrip((Button) v));
        view.findViewById(R.id.fab_history).setOnClickListener(v -> mListener.onHistoryRequested(mLocationUtils.getAllTrips()));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnTrackFragmentInteraction) {
            mListener = (OnTrackFragmentInteraction) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnTrackFragmentInteraction");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMapUtil = new MapUtil(googleMap);
        mLocationUtils.setMap(mMapUtil);
        getLastLocation();
        //    startTrip();
//        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    public void getLastLocation() {
        if (((TrackActivity) getActivity()).mLocationPermissionGranted) {
            try {
                Task<Location> currentLocationTask = mLocationClient.getLastLocation();
                currentLocationTask.addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        mLastKnownLocation = task.getResult();
                        if (mLastKnownLocation != null) {
                            mMapUtil.getMap().setMyLocationEnabled(true);
                            mMapUtil.getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(mLastKnownLocation.getLatitude(),
                                            mLastKnownLocation.getLongitude()), MapUtil.DEFAULT_ZOOM));
                            continueTrip();
                            //LatLngTime latLngTime = new LatLngTime(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude(), mLastKnownLocation.getTime());
                            //mMapUtil.addMarkerAndMove(latLngTime, "Current Location");
                            //  mMap.mark
                        }

                    } else {
                        Log.e(TAG, "Exception: %s", task.getException());
                    }
                });

            } catch (SecurityException e) {
                Log.e(TAG, "Exception: %s", e);
            }
        }
    }

    private void startStopTrip(Button view) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ((TrackActivity) getActivity()).requestLocationPermission();
            return;
        }
        if (!mLocationUtils.isTripOnGoing()) {
            view.setText(R.string.stop_trip);
            mLocationUtils.startTrip();
            mLocationClient.requestLocationUpdates(mLocationUtils.getLocationRequest(), mLocationUtils.mLocationCallBack, null);
        } else {
            view.setText(R.string.start_trip);
            mLocationUtils.stopTrip();
            mLocationClient.removeLocationUpdates(mLocationUtils.mLocationCallBack);
        }
    }

    private void continueTrip() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ((TrackActivity) getActivity()).requestLocationPermission();
            return;
        }
        if (mLocationUtils.isTripOnGoing()) {
            mLocationUtils.continueTrip(mLastKnownLocation);
            mLocationClient.requestLocationUpdates(mLocationUtils.getLocationRequest(), mLocationUtils.mLocationCallBack, null);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mLocationUtils.setMap(null);
    }

    @Override
    public void onResume() {
        super.onResume();
        mLocationUtils.setMap(mMapUtil);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLocationUtils.stopTrip();
    }

    public void updatePath(LatLngTime origin, LatLngTime dest, Route route) {
        mLocationUtils.updatePaths(origin, dest, route);
    }

    public LocationUtils getLocationUtils() {
        return mLocationUtils;
    }

    public interface OnTrackFragmentInteraction {
        void onHistoryRequested(ArrayList<Trip> trips);
    }
}
