package com.maxx.movementtracker.Network;

import android.content.Context;

import com.maxx.movementtracker.model.Direction;
import com.maxx.movementtracker.model.LatLngTime;
import com.maxx.movementtracker.model.Trip;

import retrofit2.Retrofit;

/**
 * @author Mayank Gautam
 *         Created: 02/12/17
 */

public class NetworkUtils {

    private static String BASE_URL = "http://test.dgtest.org";
    private static String MAPS_URL = "https://maps.googleapis.com";

    public static void sendTrip(Trip trip, OnLoadFinishListener listener){
        Retrofit re = RetrofitClient.getClient(BASE_URL,(Context) listener);
        ApiService apiService = re.create(ApiService.class);
        apiService.postTrip(trip.locations).enqueue(new NetworkCallback<Void>(listener).getCallback());
    }

    public static void getDirections(LatLngTime origin, LatLngTime dest, OnLoadFinishListener listener) {
        Retrofit re = RetrofitClient.getClient(MAPS_URL, (Context) listener);
        String originStr = origin.getLatitude() + "," + origin.getLongitude();
        String destStr = dest.getLatitude() + "," + dest.getLongitude();
        ApiService apiService = re.create(ApiService.class);
        apiService.getDirections(originStr, destStr).enqueue(new NetworkCallback<Direction>(listener, origin, dest).getCallback());
    }
}
