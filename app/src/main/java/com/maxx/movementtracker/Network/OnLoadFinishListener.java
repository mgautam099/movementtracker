package com.maxx.movementtracker.Network;

import com.maxx.movementtracker.model.NetworkError;

import retrofit2.Response;

/**
 * @author Mayank Gautam
 *         Created: 02/12/17
 */

public interface OnLoadFinishListener {

    void onCallComplete(Response result, Object[] params);

    void onError(NetworkError error);

}
