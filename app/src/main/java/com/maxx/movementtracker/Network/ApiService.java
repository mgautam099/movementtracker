package com.maxx.movementtracker.Network;

import com.maxx.movementtracker.model.Direction;
import com.maxx.movementtracker.model.LatLngTime;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * @author Mayank Gautam
 *         Created: 02/12/17
 */

public interface ApiService {
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("/direction/")
    Call<Void> postTrip(@Body List<LatLngTime> coordinates);

    @GET("/maps/api/directions/json?sensor=false")
    Call<Direction> getDirections(@Query("origin") String origin,
                                  @Query("destination") String destination);
}
