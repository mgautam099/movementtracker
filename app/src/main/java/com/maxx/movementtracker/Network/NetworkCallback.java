package com.maxx.movementtracker.Network;

import android.support.annotation.NonNull;

import com.maxx.movementtracker.model.NetworkError;

import java.net.SocketTimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author Mayank Gautam
 *         Created: 02/12/17
 */

public class NetworkCallback<T> {
    public static final String TYPE_LOGIN = "user_login";
    public static final String TYPE_REGISTER = "new_user";
    public static final String TYPE_GET_PARAM_LIST = "params";
    public static final String TYPE_GET_BRANCH_LIST = "branches";
    public static final String TYPE_GET_DETAIL_LIST = "details";
    private OnLoadFinishListener mListener;
    private Object[] mParams;

    /**
     * @param listener
     */
    public NetworkCallback(OnLoadFinishListener listener, Object... params) {
        mListener = listener;
        mParams = params;
    }

    private static NetworkError getErrorBody(Throwable t) {
        NetworkError e = new NetworkError();
        if (t instanceof SocketTimeoutException) {
            e.setName(NetworkError.SOCKET_TIMEOUT);
            e.setDescr("Connection Timed out, please check your internet connection and try again.");
        } else {
            e.setName(t.getMessage());
            e.setDescr(t.getLocalizedMessage());
        }
        return e;
    }

    public Callback<T> getCallback() {
        return new Callback<T>() {
            @Override
            public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
                mListener.onCallComplete(response, mParams);
            }

            @Override
            public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
                NetworkError e = getErrorBody(t);
                mListener.onError(e);
            }
        };
    }
}
