package com.maxx.movementtracker;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.maxx.movementtracker.Network.OnLoadFinishListener;
import com.maxx.movementtracker.fragment.TrackFragment;
import com.maxx.movementtracker.fragment.TripFragment;
import com.maxx.movementtracker.fragment.TripHistoryFragment;
import com.maxx.movementtracker.model.Direction;
import com.maxx.movementtracker.model.LatLngTime;
import com.maxx.movementtracker.model.NetworkError;
import com.maxx.movementtracker.model.Trip;

import java.util.ArrayList;

import retrofit2.Response;

public class TrackActivity extends AppCompatActivity implements TripHistoryFragment.TripInteractionListener,
        TrackFragment.OnTrackFragmentInteraction, OnLoadFinishListener {

    private static final int PERMISSION_REQUEST_FINE_LOCATION = 1;

    //Variable to maintain if location permission has been granted by user
    public boolean mLocationPermissionGranted = false;
    TrackFragment trackFragment;
    private String TAG = "TrackActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track);
        // setSupportActionBar(findViewById(R.id.toolbar));
        requestLocationPermission();
        init();
    }

    private void init() {
        trackFragment = new TrackFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fragment, trackFragment);
        fragmentTransaction.commit();
    }

    /**
     * Request the location permission required for tracking user location.
     * The result of the request will be handled by callback, {@link #onRequestPermissionsResult}
     */
    public void requestLocationPermission() {
        String permission = Manifest.permission.ACCESS_FINE_LOCATION;
        if (ContextCompat.checkSelfPermission(this,
                permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                new AlertDialog.Builder(this)
                        .setMessage("Please grant location permission needed to capture your movement through this app")
                        .setNeutralButton("OK", (dialog, which) ->
                                ActivityCompat.requestPermissions(this, new String[]{permission},
                                        PERMISSION_REQUEST_FINE_LOCATION))
                        .show();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{permission},
                        PERMISSION_REQUEST_FINE_LOCATION);
            }
        } else {
            onPermissionGranted();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        String permission = Manifest.permission.ACCESS_FINE_LOCATION;
        if (requestCode == PERMISSION_REQUEST_FINE_LOCATION) {
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                onPermissionGranted();
            } else {
                if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                        permission)) {
                    new AlertDialog.Builder(this).setMessage("This app needs Location permission to run, please provide the application. App will now exit!")
                            .setNeutralButton("OK", (dialog, which) -> finish()).setCancelable(false).show();
                } else {
                    requestLocationPermission();
                }
            }
        }
    }

    /**
     * Called in case user grants the permission to access location.
     */
    private void onPermissionGranted() {
        mLocationPermissionGranted = true;
        if (trackFragment != null)
            trackFragment.getLastLocation();
    }

    public void replaceFragment(Fragment fragment, boolean addToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment, fragment);
        if (addToBackStack)
            fragmentTransaction.addToBackStack(fragment.toString());
        fragmentTransaction.commit();
    }

    public Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.fragment);
    }

    public void showSnack(View view, String message) {
        try {
            Snackbar.make(view, message, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        } catch (Exception ignored) {

        }
    }

    @Override
    public void onTripClicked(Trip trip) {
        TripFragment tripFragment = TripFragment.newInstance(trip);
        replaceFragment(tripFragment,true);
    }

    @Override
    public void onHistoryRequested(ArrayList<Trip> trips) {
        if(trips.size()==0){
            Toast.makeText(this,"No Trips to display",Toast.LENGTH_SHORT).show();
        } else{
            TripHistoryFragment tripHistoryFragment = TripHistoryFragment.newInstance(trips);
            replaceFragment(tripHistoryFragment,true);
        }
    }

    /**
     * Called after a network call has been completed. This method will be called even if the call
     * completed but there was an error response from server.
     *
     * @param response a {@link okhttp3.ResponseBody} returned by retrofit for further processing of result
     */
    @Override
    public void onCallComplete(Response response, Object... params) {
        //If result successful proceed to inform the user.
        if (response.isSuccessful()) {
            if (params != null && params.length == 2) {
                if (params[0] instanceof LatLngTime) {
                    if (trackFragment != null && response.body() != null && response.body() instanceof Direction)
                        //Only the first route is required as of now.
                        trackFragment.updatePath((LatLngTime) params[0], (LatLngTime) params[1], ((Direction) response.body()).routes.get(0));
                }
            } else
                Toast.makeText(this, "Successfully posted data", Toast.LENGTH_SHORT).show();
        }
        else {
            //Check if an error code was returned by server and show it to the User.
            Toast.makeText(this, "Posting failed: " + response.message(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Called if network call failed to complete.
     * Typical error in this case will be {@link java.net.SocketTimeoutException}.
     * @param error {@link NetworkError} object containing the error returned by retrofit library.
     *
     */
    @Override
    public void onError(NetworkError error) {
        Toast.makeText(this, error.getDescr(), Toast.LENGTH_LONG).show();
    }

    /**
     * A wrapper for onBackPressed. This method is called when user presses the actionbar back button.
     * @param view the clicked button
     */
    public void backPressed(View view){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0 && trackFragment.getLocationUtils().isTripOnGoing()) {
            new AlertDialog.Builder(this)
                    .setMessage("Do you want to exit? This will stop and save the current trip")
                    .setPositiveButton("Yes", (dialog, which) -> {
                        super.onBackPressed();
                    })
                    .setNeutralButton("Minimize", (dialog, which) -> {
                        this.moveTaskToBack(true);
                    })
                    .setNegativeButton("No", null)
                    .show();
        } else
            super.onBackPressed();
    }
}
