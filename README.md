# README #

Movement tracker provides intuitive interface to track user’s path and store them for future viewing.

### Features of the App ###

* Tracks user’s movement through GPS and Google’s Location API and traces them on the Map using Google’s Maps API.
* Works offline/online seamlessly.
* Trips are tracked in foreground as well as background (on minimizing app).
* Ongoing trips are saved at regular intervals – At Start, every 20 seconds and at end.
* On force closure (by system/user), app resumes the journey from last saved point. 
* If the difference in distance and time between last saved location and the new location is considerable (200m and 40s resp.), the app tries to find the lost points using Google’s Directions API.
* Stopping the trip prompts the app to post the data to the server and the response is displayed to the user.
* Due to the time-bound nature of data, the trips are saved in serialized format (as JSON) on the internal storage of the device and not the database.
* The trip history is retrieved from storage and is shown to the user whenever they open the app.
* Clicking on any trip from history will plot it on a separate map without disturbing the ongoing trip, if any.
* An approximation of total distance travelled is calculated for each trip and displayed to the user in trip history list.

### Requirements ###

* For Mobile app:
	1. An Android device with 4.1 or above.
	2. Google play services installed on the device.
	3. GPS enabled on the device.
	4. (Optional) Working internet connection (required for plotting the trip on the map).
	
* For Development Code:
	1. Android Studio 3.0+ with Gradle 4.1+
	2. Android SDK version 26(Oreo) installed with build tools version 27.0.1.
	3. Google API Key.